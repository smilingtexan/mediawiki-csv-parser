# MediaWiki CSV Parser

Will parse CSV files into a MediaWiki table.

To install, place the CSV_Parser folder in your MediaWiki extensions folder and enable in the LocalSettings.php file.

This extension responds to the MediaWiki keyword: csv_parse
You will need to specify the 'directory' and 'filename' arguments so that the extension will know what file to open. The directory should be accessible from the web-site current working directory (cwd).

In the CSV file, the top line is the table header. CSV_Parser will repeat this line every 25 rows. Make sure to use commas (,) for the delimiter. In each cell, you may use the semicolon (;) for a new line. Multiple spaces may also cause issues.

```
cd existing_repo
git remote add origin https://gitlab.com/smilingtexan/mediawiki-csv-parser.git
git branch -M main
git push -uf origin main
```

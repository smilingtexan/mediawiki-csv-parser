This parser registers the keyword: csv_parse

It will want a "filename" argument as well as a "directory" argument. The directory should end with a slash "/".

This will parse the CSV file into a wiki-table format for display on a MediaWiki page.

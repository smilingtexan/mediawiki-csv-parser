<?php
// The following is a "non-public" file that stores the username/password data for accessing any SQL tables, or private file servers
// You probably don't need this type access, but if you do, you may uncomment it
//require_once('../includes/db_connect.php');

class CSV_Parser {
	/**
 	 * Called when the extension is first loaded
	 */
	public static function onRegistration()
	{
		global $wgExtensionFunctions;
		$wgExtensionFunctions[] = __CLASS__ . '::setup';
	}

	public static function setup()
	{
		global $wgOut, $wgResourceModules, $wgExtensionAssetsPath, $IP, $wgAutoloadClasses;

		$parser = \MediaWiki\MediaWikiServices::getInstance()->getParser();

		// Register the parser-function
		$parser->setFunctionHook( 'csv_parse', __CLASS__ . '::parseGeneric' );

		// This gets the remote path even if it's a symlink (MW1.25+)
		$path = $wgExtensionAssetsPath . str_replace( "$IP/extensions", '', dirname( $wgAutoloadClasses[__CLASS__] ) );
		$wgResourceModules['ext.tmc_dms_parts']['remoteExtPath'] = $path;
		$wgOut->addModules( 'ext.tmc_dms_parts' );
	}

	private static function insertHeaderRow($header)
	{
		$count = 0;
		$size = sizeof($header);
		$var = "\n|- style='background-color:#FFF;color:#000;'\n";
		foreach($header as $head)
		{
			$var .= "|'''{$head}'''";
			$count++;
			if ($count < $size) { $var .= "|"; }
		}
		return($var);
	}

	private static function getWikiTableHeader($header)
	{
		$var = "<br/>\n{| class='wikitable'";
		$var .= self::insertHeaderRow($header);
		$var .= "\n";
		return($var);
	}

	private static function getWikiTableFooter()
	{
		return("\n|}\n</div>");
	}

	private static function insert_newcell()
	{
		return("\n|");
	}

	private static function insert_newrow($odd)
	{
		$var = ("\n|- ");
		if ($odd) { $var .= " style='background-color:#EEE;color:#000;'"; }
		else { $var .= " style='background-color:#888; color:#EEE;'"; }
		$var .= "\n";
		return($var);
	}

	public static function parseGeneric ( $parser )
	{
		$parser->getOutput()->updateCacheExpiry(0);

		$title = "";
		$argv = array();
		foreach ( func_get_args() as $arg )
		{
			if ( !is_object( $arg ) )
			{
				if ( preg_match( '/^(.+?)\s*=\s*(.+)$/', $arg, $match ) )
				{
					$argv[$match[1]] = $match[2];
				}
			}
		}
		$filename = getcwd() . '/misc/released/';
		if (array_key_exists('directory',$argv)) { $filename = getcwd() . $argv['directory']; }
		if(!array_key_exists('filename',$argv)) { return array( "<br/>Please specify a filename for table!<br/>", 'isHTML' => false, 'noparse' => false ); }
		else { $filename .= $argv['filename']; }
		if (!file_exists($filename)) { return array( "<br/>File specified ({$filename}) does not exist!<br/>", 'isHTML' => false, 'noparse' => false ); }
		if(array_key_exists('title',$argv)) { $title = $argv['title']; }

		$retStr = "==" . $title . "==";
		$row = 1;
		$handle = fopen($filename, "r");
		if ($handle !== FALSE)
		{
			$fileDate = filemtime($filename);
			$fileDateStr = date("d-M-Y H:i", $fileDate);
			$retStr .= "<br/>Table last edited: {$fileDateStr}<br/>\n{| class='wikitable' style='font-size:9pt'\n|-\n";
			$dataArray = array();
			$header = array();
			while (($data = fgetcsv($handle, 200, ",")) !== FALSE)
			{
				if ($row == 1) { $header = $data; }
				$num = count($data);
				for ($c=0; $c<$num; $c++)
				{
					$dataArray = explode(";", $data[$c]);
					$retStr .= "|| ";
					if (($row > 1) && (!empty($dataArray[0])))
					{
							$prefix = substr($dataArray[0], 0, 2);
							foreach($dataArray as $str)
							{
									$retStr .= $str . "<br/>";
							}
					}
					else { $retStr .= $data[$c]; }
				}
				$retStr .= "\n|-\n";
				$row++;
				if (($row % 25) == 0)
				{
					$num = count($header);
					for ($c=0; $c<$num; $c++)
					{
						$retStr .= "| " . $header[$c] . "\n";
					}
					$retStr .= "\n|-\n";
				}
			}
			fclose($handle);
			$retStr .= "|}\n";
		}
		return array( $retStr, 'isHTML' => false, 'noparse' => false );
	}

}
